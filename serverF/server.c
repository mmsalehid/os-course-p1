#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>


#define serverPort 9018
#define heartBeatMassage "9018"
#define DELAY 1
#define LOCALHOST "127.0.0.1"
#define MAXIMUM_PARTICIPANTS 20
#define BUFFER_SIZE 256
#define UPLOAD 0
#define DOWNLOAD 1

int broadCastSocket;
struct sockaddr_in broadCastAddr;

struct file_info{
    char fileName[BUFFER_SIZE];
    int task;   //0 for upload to server and 1 to download from server
    int fileSize;
} file_info;



void heartBeatHandler(int signum){
    if (signum == SIGALRM)
    {
        if ((sendto(broadCastSocket, heartBeatMassage, strlen(heartBeatMassage),
         0, (struct sockaddr *) &broadCastAddr, sizeof(broadCastAddr))) < 0){
            write(1,"Heartbeat sending failed\n",strlen("Heartbeat sending failed\n"));  
            exit(EXIT_FAILURE);
        }   
        alarm(DELAY);
    }
}

int areEqual(char *string1, char *string2)
{
    int string1Size, string2Size;
    int index;
    string1Size = strlen(string1);
    string2Size = strlen(string2);
    if (string1Size != string2Size)
        return 0;
    for (index = 0; index < string1Size; index++)
        if (string1[index] != string2[index])
            return 0;

    return 1;
}

int saveUploadedFile(int clientSocket){
    char* buff;
    int i = 0;
    int fd;
    int isRead;
    char inputChar;
    char fileName[BUFFER_SIZE];
    buff = (char* )malloc(sizeof(char));
    read(clientSocket,fileName,BUFFER_SIZE);
    printf("%s\n",fileName);
    if ((fd = open(fileName,O_CREAT | O_WRONLY,S_IRUSR | S_IWUSR)) < 0){
        write(1, "cannot make uploaded file\n", strlen("cannot make uploaded file\n"));
        return 0;
    } 
    memset(buff, '\0', i);
    buff = realloc(buff,strlen("send your file"));
    strcat(buff,"send your file");
    int test = write(clientSocket,buff, strlen(buff));
    memset(buff, '\0', i);
    int nbytes;
    i = 0;
    buff = (char*)realloc(buff, BUFFER_SIZE* sizeof(char));
    while(1){
        nbytes = read(clientSocket,buff,BUFFER_SIZE);
        if(nbytes < BUFFER_SIZE){
        buff = (char*)realloc(buff, i*BUFFER_SIZE + nbytes);
        break;
        }
        else{
            i++;
            buff = (char*) realloc(buff,i*BUFFER_SIZE*sizeof(char));
        }
    }
    if(write(fd,buff,strlen(buff)) < 0){
        write(1,"File did not save properly\n", strlen("File did not save properly\n"));
        close(fd);
        return 0;
    }
    close(fd);
    return 1;
}   

int sendFile(int clientSocket){
    char *buff;
    int i = 0;
    char fileName[BUFFER_SIZE];
    int fd;
    int nbytes;
    read(clientSocket,fileName,BUFFER_SIZE);
    if ((fd = open(fileName, O_WRONLY,0)) < 0){
        write(clientSocket,"this file is not in the server",strlen("this file is not in the server"));
        write(1, "this file is not in the server\n", strlen("this file is not in the server\n"));
        return 0;
    }
    buff = (char*)malloc(sizeof(char)*BUFFER_SIZE);
    while(1){
        nbytes = read(fd,buff,BUFFER_SIZE);
        if(nbytes < BUFFER_SIZE){
            buff = (char*)realloc(buff, i*BUFFER_SIZE + nbytes);
            break;
        }
        else{
            i++;
            buff = (char*) realloc(buff,i*BUFFER_SIZE*sizeof(char));
        }
    }
    write(clientSocket,buff,strlen(buff));

}
int main(int argc, char *argv[]){
    struct sockaddr_in serverAddress, clientAddress;
    int serverSocket, newSocket;
    socklen_t clientAddrLen = sizeof(clientAddress);
    fd_set readFdSet;
    fd_set activeFdSet;
    int fdIndex;
    char buffer[BUFFER_SIZE];
    //Initializing broadcast socket for heartbeat
    unsigned short heartBeatPort;
    int yes = 1;
    if (argc < 2){
        write(1, "Arguments format are not valid!", strlen("Arguments format are not valid!"));
    }
    heartBeatPort = atoi(argv[1]);
    if ((broadCastSocket = socket(AF_INET,SOCK_DGRAM,0)) < 0 )
    {
        write(1, "Server failed to stablish broadcasting\n", strlen("Server failed to stablish broadcasting\n"));
        exit(0);
    }
    broadCastAddr.sin_family = AF_INET;
    broadCastAddr.sin_addr.s_addr = INADDR_BROADCAST;
    broadCastAddr.sin_port = htons(heartBeatPort);
    setsockopt(broadCastSocket, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
    signal(SIGALRM,heartBeatHandler);
    alarm(DELAY);
    //Finish initializing broadcast socket for heartbeat
    //Initializing server stream socket
    if((serverSocket = socket(AF_INET, SOCK_STREAM,0)) < 0){
        write(1, "Could not open a socket for server\n", strlen("Could not open a socket for server\n"));
        exit(0);
    }
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(LOCALHOST);
    serverAddress.sin_port = htons(serverPort);
    setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &yes ,sizeof(yes));
    struct timeval waitTime;
    waitTime.tv_sec = 2;
    waitTime.tv_usec = 0;
    setsockopt(serverSocket,SOL_SOCKET,SO_RCVTIMEO, &waitTime, sizeof(&waitTime));
    if (bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        write(1, "Could not bind to that port\n", strlen("Could not bind to that port\n"));
        exit(1);
    }
    listen(serverSocket,MAXIMUM_PARTICIPANTS);
    //Finish initializing server stream socket
    FD_ZERO(&readFdSet);
    FD_ZERO(&activeFdSet);
    FD_SET(serverSocket, &activeFdSet);
    FD_SET(0, &activeFdSet);
    int maxFD = serverSocket;
    struct timeval selectTime;
    selectTime.tv_sec = 0;
    selectTime.tv_usec = 0;
    while (1){
        readFdSet = activeFdSet;
        if(select(maxFD+1,&readFdSet,NULL,NULL,&selectTime) < 0){
            if(errno == EINTR){
                continue;
            }
            else{
                write(1, "Select could not be done\n", strlen("Select could not be done\n"));
                exit(EXIT_FAILURE);
            }
        }
        for (fdIndex = 0; fdIndex <= maxFD; fdIndex++){
            if(FD_ISSET(fdIndex, &readFdSet)){
                if (fdIndex == serverSocket){
                    printf("check\n");
                    if((newSocket = accept(serverSocket, (struct sockaddr *)&clientAddress, &clientAddrLen)) < 0)
                    {
                        write(1,"Could not connect to new socket\n",strlen("Could not connect to new socket\n"));
                    }
                    else {
                        write(1,"A client is connecting to server\n",strlen("A client is connecting to server\n"));
                        FD_SET(newSocket,&activeFdSet);
                        if(maxFD < newSocket)
                            maxFD = newSocket;
                    }
                }
                else if (fdIndex == 0){
                    memset(buffer, '\0', BUFFER_SIZE);
                    read(fdIndex, buffer, BUFFER_SIZE);
                    if(areEqual(buffer,"exit\n"))
                    {
                        write(1,"Server shoot down correctly\n",strlen("Server shoot down correctly\n"));
                        close(serverSocket);
                        exit(EXIT_SUCCESS);
                    }
                    else
                        write(1,"You entered wrong instruction\n",strlen("You entered wrong instruction\n"));
                }
                else{
                    memset(buffer, '\0', BUFFER_SIZE);
                    read(fdIndex, buffer,1);
                    if(areEqual(buffer,"U")){
                        write(1,"Uploading file from a clinet to server\n",strlen("Uploading file from a clinet to server\n"));
                        saveUploadedFile(fdIndex);
                    }
                    if(areEqual(buffer,"D")){
                        write(1,"A client is downloading file from server\n",strlen("A client is downloading file from server\n"));
                        sendFile(fdIndex);
                    }
                }
            }
        }
    }
    return 0;
}