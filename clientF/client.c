
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <sys/signal.h>
#include <unistd.h>  
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define DELAY 2
#define LOCALHOST "127.0.0.1"
#define BUFFER_SIZE 256
#define UPLOAD '0'
#define DOWNLOAD '1'


struct file_info{
    char fileName[BUFFER_SIZE];
    int task;   //0 for upload to server and 1 to download from server
    int fileSize;
};

unsigned int heartBeatChecker(int heartBeatSocket,struct sockaddr_in heartBeatAddr){
    char buff[255];
    memset(buff, '\0', 255);
    socklen_t heartBeatAddrLen = sizeof(heartBeatAddr);
    write(1,"Checking if server is alive...\n",strlen("Checking if server is alive...\n"));
    if(recvfrom(heartBeatSocket,buff,sizeof(buff),0,
                (struct sockaddr*)&heartBeatAddr,&heartBeatAddrLen) > 0)
    {   
        return atoi(buff);
    }
    else return -1;
} 

char* deleteEnter(char *str, char* buff){
    int index = 0;
    while (str[index] != '\n' && str[index] != '\0'){
        buff[index] = str[index];
        index++;
    }
    buff[index] = '\0';
    return buff;
}

//check if str1 is first of str2
int isFirstOfStr(char *string1, char *string2){
    int string1Size, string2Size;
    int index;
    string1Size = strlen(string1);
    string2Size = strlen(string2);
    if (string2Size < string1Size){
        return 0;
    }
    else {
        for (index = 0; index < string1Size; index++ ){
            if (string1[index] != string2[index]){
                return 0;
            }
        }
        return 1;
    }
}

char* strSubtract(char *str1, char *str2, char *buff){
    int str2Size = strlen(str2);
    int str1Size = strlen(str1);
    int index;
    if (!isFirstOfStr(str1,str2))
        return '\0';
    else {
        for (index = str1Size; index < str2Size; index++){
            buff[index - str1Size] = str2[index];
        }
        buff[index-str1Size] = '\0';
        return buff;
    }
}

int areEqual(char *string1, char *string2)
{
    int string1Size, string2Size;
    int index;
    string1Size = strlen(string1);
    string2Size = strlen(string2);
    if (string1Size != string2Size)
        return 0;

    for (index = 0; index < string1Size; index++)
        if (string1[index] != string2[index])
            return 0;

    return 1;
}

int uploadFile(int serverSocket,char* fileName){
    int fd;
    char *buff;
    int i = 0;
    int isRead;
    char inputChar;
    buff = (char*)malloc(strlen("U"));
    strcat(buff,"U");
    write(serverSocket,buff,strlen(buff));
    memset(buff, '\0', strlen("U"));
    buff = (char*)realloc(buff,strlen(fileName)+1);
    strcat(buff, fileName);
    write(serverSocket, buff, strlen(fileName)+1);
    memset(buff, '\0', strlen(buff));
    buff = (char*)realloc(buff,strlen("send your file"));
    read(serverSocket,buff,strlen("send your file"));
    memset(buff, '\0', strlen("send your file"));
    if((fd = open(fileName,O_RDONLY,S_IRUSR | S_IWUSR))<0)
    {
        write(1,"Could not open this file\n",strlen("Could not open this file\n"));
        return -1;
    }
    else{
        buff = (char*)realloc(buff,sizeof(char));
        while(1){
        i++;
        buff = (char*) realloc(buff,sizeof(char)* i);
        isRead = read(fd, &inputChar, 1);
        buff[i-1] = inputChar;
        if(isRead == 0)
            break;
    }
    }
    write(serverSocket,buff,strlen(buff));
    memset(buff, '\0', strlen(buff));
}

int downloadFile(int serverSocket,char* fileName){
    int fd;
    char *buff;
    int i = 0;
    int isRead;
    char inputChar;
    int nbytes;
    buff = (char*)malloc(strlen("D"));
    strcat(buff,"D");
    write(serverSocket,buff,strlen(buff));
    memset(buff, '\0', strlen("D"));
    buff = (char*)realloc(buff,strlen(fileName)+1);
    strcat(buff, fileName);
    write(serverSocket, buff, strlen(fileName)+1);
    memset(buff, '\0', strlen(buff));
    buff = (char*) realloc(buff,BUFFER_SIZE*sizeof(char));
    memset(buff,'\0',BUFFER_SIZE);
    while(1){
        nbytes = read( serverSocket,buff,BUFFER_SIZE);
        if(nbytes < BUFFER_SIZE){
            buff = (char*)realloc(buff, i*BUFFER_SIZE + nbytes);
            break;
        }
        else{
            i++;
            buff = (char*) realloc(buff,i*BUFFER_SIZE*sizeof(char));
        }
    }
    printf("%ld\n",strlen(buff));
    if(areEqual(buff,"this file is not in the server")){
        //will be completed
        return 0;
    }
    if ((fd = open(fileName,O_CREAT | O_WRONLY,S_IRUSR | S_IWUSR)) < 0){
        write(1, "cannot make downloaded file\n", strlen("cannot make downloaded file\n"));
        return 0;
    } 
    write(fd,buff,strlen(buff));
    return 1;
}


int main(int argc, char *argv[]){
    struct sockaddr_in serverAddr, clientAddr;
    int serverSocket = -1, clientSocket, newSocket;
    socklen_t clinetAddrLen;
    unsigned short clientPort, serverPort;
    fd_set readFdSet,activeFdSet;
    int fdIndex;
    char buffer[BUFFER_SIZE];
    char fileName[BUFFER_SIZE];
    struct file_info fileInfo;
    int isServerAlive;
    char* file_buffer;
    if (argc < 4){
        write(1, "Arguments format are not valid!", strlen("Arguments format are not valid!"));
    }

    //Listen to heartbeat broadcast
    int heartBeatSocket;
    struct sockaddr_in heartBeatAddr;
    struct timeval checkServerDelay;

    int yes = 1;
    unsigned short heartBeatPort = atoi(argv[1]);
    if ((heartBeatSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        write(1, "Could not stablish a connection for broadcasting\n", strlen("Could not stablish a connection for broadcasting\n"));
        exit(0);
    }
    setsockopt(heartBeatSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
    checkServerDelay.tv_sec = DELAY;
    checkServerDelay.tv_usec = 0;
    setsockopt(heartBeatSocket, SOL_SOCKET, SO_RCVTIMEO, &checkServerDelay,sizeof(checkServerDelay));

    heartBeatAddr.sin_family = AF_INET;
    heartBeatAddr.sin_addr.s_addr = INADDR_ANY;
    heartBeatAddr.sin_port = htons(heartBeatPort);
    unsigned int heartBeatAddrLen = sizeof(heartBeatAddr);
    if (bind(heartBeatSocket, (struct sockaddr *)&heartBeatAddr, sizeof(heartBeatAddr)) < 0){
        write(1,"Could not bind to that port for listening\n", strlen("Could not bind to that port for listening\n"));
    }
    // finish heartBeat init

    // initializing client stream socket
    clientPort = atoi(argv[3]);
    if ((clientSocket = socket(AF_INET,SOCK_STREAM,0)) < 0){
        write(1,"Could not create client socket\n",strlen("Could not create client socket\n"));
        exit(EXIT_FAILURE);
    }
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_addr.s_addr = inet_addr(LOCALHOST);
    clientAddr.sin_port = htons(clientPort);
    if((bind(clientSocket, (struct sockaddr*)&clientAddr,sizeof(clientAddr))) < 0){
        write(1,"client socket could not bind to port\n",strlen("client socket could not bind to port\n"));
    }
    //finish initializing client stream socket

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(LOCALHOST);

    serverPort = heartBeatChecker(heartBeatSocket, heartBeatAddr);
    if (serverPort !=(unsigned short) -1){
        isServerAlive = 1;
        write(1,"Server is alive\n",strlen("Server is alive\n"));
        serverAddr.sin_port = htons(serverPort);
        serverSocket = socket(AF_INET,SOCK_STREAM,0);
    }
    else{ 
        isServerAlive = 0;
        write(1,"Server is not alive\n", strlen("Server is not alive\n"));
    }
    FD_ZERO(&readFdSet);
    FD_ZERO(&activeFdSet);
    if(isServerAlive)
        FD_SET(serverSocket, &activeFdSet);
    FD_SET(clientSocket, &activeFdSet);
    FD_SET(0, &activeFdSet);
    int maxFD = clientSocket;
    struct timeval selectTime;
    selectTime.tv_sec = 0;
    selectTime.tv_usec = 0;
    while (1){
        readFdSet = activeFdSet;
        if((select(maxFD+1,&readFdSet,NULL,NULL,&selectTime)) < 0){
            write(1, "Select could not be done\n", strlen("Select could not be done\n"));
        }
        for(fdIndex = 0; fdIndex <= maxFD; fdIndex++){
            if(FD_ISSET(fdIndex, &readFdSet)){
                if (fdIndex == 0){
                    memset(buffer, '\0', BUFFER_SIZE);
                    read(fdIndex,buffer,BUFFER_SIZE);
                    if(isFirstOfStr("download ",buffer)){
                        deleteEnter(strSubtract("download ",buffer,fileName), fileName);
                        if(isServerAlive){
                            if(connect(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr))<0){
                                write(1,"could not connect to server\n",strlen("could not connect to server\n"));
                                close(clientSocket);
                                exit(EXIT_FAILURE);
                            }
                            downloadFile(serverSocket,fileName);
                        }
                    }
                    else if(isFirstOfStr("upload",buffer)){
                        deleteEnter(strSubtract("upload ",buffer,fileName), fileName);
                        if(isServerAlive){
                            if(connect(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr))<0){
                                write(1,"could not connect to server\n",strlen("could not connect to server\n"));
                                close(clientSocket);
                                exit(EXIT_FAILURE);
                            }
                            uploadFile(serverSocket,fileName);
                        }
                    }
                    else if(areEqual("exit\n",buffer)){
                        write(1,"Client shoot down correctly\n",strlen("Client shoot down correctly\n"));
                        close(clientPort);
                        exit(EXIT_SUCCESS);
                    }
                    else{
                        write(1,"You entered wrong instruction\n",strlen("You entered wrong instruction\n"));
                    }
                }
            }
        }
    }
    return 0;
}